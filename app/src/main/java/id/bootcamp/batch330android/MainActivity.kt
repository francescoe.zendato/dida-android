package id.bootcamp.batch330android

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import id.bootcamp.batch330android.activitystack.Stack1Activity
import id.bootcamp.batch330android.registration.RegistrationFormActivity

class MainActivity : AppCompatActivity() { //halaman 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //set layout untuk MainActivity menggunakan activity_main.xml
        setContentView(R.layout.activity_main)

        //ambil objek button
        val btnStackActivity = findViewById<Button>(R.id.btnActivityStack)
        val btnRegister = findViewById<Button>(R.id.btnRegister)

        //atur aksi button
        btnStackActivity.setOnClickListener {
//            Toast.makeText(this,"Button Stack Activity diklik! WOW!!!",
//                Toast.LENGTH_SHORT).show() //notif-ception
            val intent = Intent(this,Stack1Activity::class.java)
            startActivity(intent)
        }

        btnRegister.setOnClickListener {
            val intent = Intent(this,RegistrationFormActivity::class.java)
            startActivity(intent)
        }

        Log.d("activity_lifecycle","OnCreate Terpanggil")
    }

    override fun onStart() {
        super.onStart()
        Log.d("activity_lifecycle","OnStart Terpanggil")
    }

    override fun onResume() {
        super.onResume()
        Log.d("activity_lifecycle","OnResume Terpanggil")
    }

    override fun onPause() {
        super.onPause()
        Log.d("activity_lifecycle","OnPause Terpanggil")
    }

    override fun onStop() {
        super.onStop()
        Log.d("activity_lifecycle","OnStop Terpanggil")
    }

    override fun onRestart() {
        super.onRestart()
        Log.d("activity_lifecycle","OnRestart Terpanggil")
    }

    override fun onDestroy() { //bisa balapan sama logcatnya
        super.onDestroy()
        Log.d("activity_lifecycle","OnDestroy Terpanggil")
    }

    //banyakin commit
}
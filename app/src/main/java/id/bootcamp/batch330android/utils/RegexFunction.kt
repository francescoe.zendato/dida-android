package id.bootcamp.batch330android.utils

val emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+\$"

fun isValidEmail(email: String): Boolean {
    return email.matches(emailRegex.toRegex())
}
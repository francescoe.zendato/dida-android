package id.bootcamp.batch330android.registration

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.RadioButton
import android.widget.Spinner
import android.widget.Toast
import id.bootcamp.batch330android.R
import id.bootcamp.batch330android.utils.isValidEmail

class RegistrationFormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_form)

        //mengambil semua object di layout Form
        val editTextFirstName = findViewById<EditText>(R.id.eFirstName)
        val editTextLastName = findViewById<EditText>(R.id.eLastName)
        val editTextEmail = findViewById<EditText>(R.id.eEmail)
        val editTextAddress = findViewById<EditText>(R.id.eAddress)
        val radioButtonMale = findViewById<RadioButton>(R.id.radioMale)
        val radioButtonFemale = findViewById<RadioButton>(R.id.radioFemale)
        val checkBoxIndo = findViewById<CheckBox>(R.id.cIndonesia)
        val checkBoxEng = findViewById<CheckBox>(R.id.cEnglish)
        val spinnerState = findViewById<Spinner>(R.id.sState)
        val buttonSubmit = findViewById<Button>(R.id.btnSubmit)

        //mengambil array string dari resources (res.values.strings.xml)
        val states = resources.getStringArray(R.array.states)

        //mengisi dropdown/spinner
        val adapter = ArrayAdapter(this,
            android.R.layout.simple_list_item_1,
            states)

        //memasang adapter ke spinner
        spinnerState.adapter = adapter

        //clickListener buat Submit
        buttonSubmit.setOnClickListener {
            //ambil input dari user
            val firstName = editTextFirstName.text.toString()
            val lastName = editTextLastName.text.toString()
            val email = editTextEmail.text.toString()
            val address = editTextAddress.text.toString()

            val gender = if (radioButtonMale.isChecked) {
                "Male"
            } else if (radioButtonFemale.isChecked) {
                "Female"
            } else {
                ""
            }

            // list, ukuran bisa berubah
            // pakai val karena ukuran tidak diperhatikan mutabilitasnya
            val listLanguage = ArrayList<String>()
            if (checkBoxIndo.isChecked) {
                listLanguage.add("Indonesia")
            }
            if (checkBoxEng.isChecked) {
                listLanguage.add("English")
            }

            val state = spinnerState.selectedItem.toString()
            //fitur debug pakai println()

            //logic validation
            val validEmail = isValidEmail(email)
            if (firstName.isBlank()) {
                Toast.makeText(this@RegistrationFormActivity,
                    "Your first name cannot be empty",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (gender.isBlank()) {
                Toast.makeText(this@RegistrationFormActivity,
                    "Please choose at least 1 gender for administration",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (listLanguage.isEmpty()) {
                Toast.makeText(this@RegistrationFormActivity,
                    "Please choose at least 1 language for communication",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (!validEmail) {
                Toast.makeText(this@RegistrationFormActivity,
                    "Your email is invalid",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else if (address.isBlank()) {
                Toast.makeText(this@RegistrationFormActivity,
                    "Your address cannot be empty",
                    Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val intent = Intent(this,RegistrationResultActivity::class.java)

            //memasukkan tiap inputan user ke intent supaya dibawa ke Act berikutnya
            intent.putExtra("keyFirstName",firstName)
            intent.putExtra("keyLastName",lastName)
            intent.putExtra("keyGender",gender)
            intent.putExtra("keyLanguage",listLanguage)
            intent.putExtra("keyEmail",email)
            intent.putExtra("keyAddress",address)
            intent.putExtra("keyState",state)

            startActivity(intent)
        }
    }
}
package id.bootcamp.batch330android.registration

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import id.bootcamp.batch330android.R

class RegistrationResultActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration_result)

        //ambil data dari Act sebelumnya
        val firstName = intent.extras?.getString("keyFirstName") //tambah ? karena bisa null,
        val lastName = intent.extras?.getString("keyLastName") //belum ada validasi
        val gender = intent.extras?.getString("keyGender")
        val language = intent.extras?.getStringArrayList("keyLanguage") //karena list
        val email = intent.extras?.getString("keyEmail")
        val address = intent.extras?.getString("keyAddress")
        val state = intent.extras?.getString("keyState")

        //ambil objek dari layout
        val textFN = findViewById<TextView>(R.id.tFN)
        val textLN = findViewById<TextView>(R.id.tLN)
        val textGender = findViewById<TextView>(R.id.tGen)
        val textLanguage = findViewById<TextView>(R.id.tLang)
        val textEmail = findViewById<TextView>(R.id.tMail)
        val textAddress = findViewById<TextView>(R.id.tAdd)
        val textState = findViewById<TextView>(R.id.tState)

        //set text sesuai Act sebelumnya
        textFN.text = firstName
        textLN.text = lastName
        textGender.text = "Gender: $gender"
        textLanguage.text = "Language: $language"
        textEmail.text = email
        textAddress.text = address
        textState.text = state
    }
}
package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import id.bootcamp.batch330android.MainActivity
import id.bootcamp.batch330android.R

class Stack2Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack2)

        val btnMainActivity = findViewById<Button>(R.id.btnGoHomepage)

        btnMainActivity.setOnClickListener {
            val intent = Intent(this,MainActivity::class.java)
            // fungsi tambahan untuk clear semua stack sebelum balik ke Main Act
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent
                .FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }
}
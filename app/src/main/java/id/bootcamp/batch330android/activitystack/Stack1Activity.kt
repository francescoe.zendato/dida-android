package id.bootcamp.batch330android.activitystack

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import id.bootcamp.batch330android.R

class Stack1Activity : AppCompatActivity() { //hal 2, menutup mainActivity
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stack1)

        val btnStack2Activity = findViewById<Button>(R.id.btnActivityStack2)

        btnStack2Activity.setOnClickListener {
            val intent = Intent(this,Stack2Activity::class.java)
            startActivity(intent)
        }
    }
}